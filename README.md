# rop me like a hurricane -- Mega -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/mega/rop_me_like_a_hurricane)

## Chal Info

Desc: `Didn't get an alien in Area51, but I found this...`

Prereq: `Broken`

Hints:

* Is there an order to the madness?

Flag: `TUCTF{bu7_c4n_y0u_ROP_bl1ndf0ld3d?}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/rop_me_like_a_hurricane)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/rop_me_like_a_hurricane:tuctf2019
```
